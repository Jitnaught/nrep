Replaces bad `Function.Call`'s in decompiled ScriptHookVDotNet mods. Sometimes native calls will show up like `Function.Call(-12345L` when `Function.Call(Hash.SOME_NATIVE` is expected. This script will automatically replace them for you.

Run `python3 nrep.py put/your/dir/here` to fix natives in all .cs files in a directory

If you want to replace natives with the SHVDN V2 natives instead of V3, run `python3 nrep.py put/your/dir/here v2`

If you get a `can only concatenate str (not "NoneType")` error, you shouldn't use V2 natives because some natives are missing

Note: this script does not detect the SHVDN version used by the mod, so you will have to decide which natives version to use. V3 is used by default
